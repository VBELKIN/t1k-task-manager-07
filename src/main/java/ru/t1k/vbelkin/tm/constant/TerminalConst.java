package ru.t1k.vbelkin.tm.constant;

public final class TerminalConst {
    public final static String VERSION = "version";
    public final static String HELP = "help";
    public final static String ABOUT = "about";
    public final static String INFO = "info";
    public final static String EXIT = "exit";
}
